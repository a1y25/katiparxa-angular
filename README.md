# Katiparxa Angular 4 project

angular cli  1.1.3.

## Development server

ng serve

`ng generate component component-name` 
`ng generate directive|pipe|service|class|module`.

 `ng build`  
 `-prod` - production build.

 `ng test`  [Karma]

## Running end-to-end tests
`ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.