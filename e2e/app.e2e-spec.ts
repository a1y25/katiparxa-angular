import { KatiparxaAngularPage } from './app.po';

describe('katiparxa-angular App', () => {
  let page: KatiparxaAngularPage;

  beforeEach(() => {
    page = new KatiparxaAngularPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
